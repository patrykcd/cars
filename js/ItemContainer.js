export let ItemContainer = function () {
    const _ID = 'items-container'
    const _container = document.getElementById(_ID)

    this.addItem = function (items) {
        _container.appendChild(items)
    }

    this.clear = function () {
        _container.innerHTML = "";
    }
}