export let FlatMenu = function () {
    let _ID = 'menu-flat'
    let _menuFlat
    let _style

    this.addButton = function (item) {
        _menuFlat = document.getElementById(_ID)
        let newButton = document.createElement('button')
        newButton.appendChild(document.createTextNode(item))
        _menuFlat.appendChild(newButton)
        console.log(`added "${newButton.textContent}" to ${_menuFlat.id}`)
        _style = _menuFlat.children[0].style.background
    }

    this.cleanItems = function () {
        let container = document.getElementById(_id)
        container.innerHTML = "";
    }

    this.addButtonEventListener = function (eventListener) {
        for (let child of _menuFlat.children) {
            function callback() {
                eventListener(child)
            }
            child.addEventListener('click', callback);
        }
    }

    this.setActive = function (name) {
        for (let child of _menuFlat.children) {
            let style = getComputedStyle(child);
            child.style.background = _style
            if (child.innerText == name) {
                let activeBg = style.getPropertyValue('--button-hover')
                child.style.background = activeBg
            }
        }
    }
}