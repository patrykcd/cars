export let Item = function (_name, _items) {
    const _CATEGORIES = [
        "photos",
        "price",
        "doors",
        "top_speed",
        "acceleration",
        "city_consumption",
        "mass_empty",
        "max_load_capacity"
    ]

    let temp = document.getElementsByTagName("template")[0]
    let clone = temp.content.cloneNode(true);
    let name = clone.getElementById('name')
    name.innerText = _name

    let content = clone.getElementById('content')
    let img = document.createElement('img')
    img.src = `./assets/${_items[0]}.jpg`

    let spec = clone.getElementById('spec')

    for (let i = 1; i < _CATEGORIES.length; i++)
        addRow(_CATEGORIES[i], _items[i])


    function addRow(leftItem, rightItem) {
        let tr = document.createElement('tr')
        let tdLeft = document.createElement('td')
        tr.appendChild(tdLeft)
        let pLeft = document.createElement('p')
        tdLeft.appendChild(pLeft)
        pLeft.setAttribute('text', leftItem)
        let tdRight = document.createElement('td')
        tr.appendChild(tdRight)
        let pRight = document.createElement('p')
        tdRight.appendChild(pRight)
        pRight.appendChild(document.createTextNode(rightItem))
        spec.appendChild(tr)
        content.insertBefore(img, spec)
    }

    return clone
}