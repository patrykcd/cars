<!DOCTYPE html>
<html>

    <head>
        <meta charset="UTF-8">
        <meta name="author" content="Patryk Cisek">

        <link rel="stylesheet" type="text/css" media="screen" href="styles/main.css" />
        <link rel="stylesheet" type="text/css" media="screen" href="styles/top-bar.css" />
        <link rel="stylesheet" type="text/css" media="screen" href="styles/menu-3d.css" />
        <link rel="stylesheet" type="text/css" media="screen" href="styles/menu-flat.css" />
        <link rel="stylesheet" type="text/css" media="screen" href="styles/items-container.css" />
        <link rel="stylesheet" type="text/css" media="screen" href="styles/bottom-bar.css" />

        <link rel="stylesheet" type="text/css" media="screen" href="https://fontlibrary.org/face/bebas" />

        <?php require('./html/item.html'); ?>
    </head>

    <body>
        <div id="main">
            <div id="top-bar">
                <div id="primary">
                    <p>CARS</p>
                    <div id="menu-3d">
                        <a href="http://patrykcd.byethost31.com/" text="main_page"></a>
                        <button onclick="scrollBottom()" text="contact"></button>
                        <button onclick="toggleLang()" id="lang" text="language"></button>
                    </div>
                </div>
                <div id="shadow"></div>
                <div id="secondary">
                    <div id="menu-flat"></div>
                </div>
            </div>
            <div id="items-container"></div>
        </div>

        <div id="bottom-bar">
            <pre text="contact_info" id="contact">
               
            </pre>
            <iframe id="location" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3430.8655357979537!2d14.489344941864896!3d53.44947495235105!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47aa0ebdcd0941a9%3A0x2ff69b899587c3a4!2sZachodniopomorska+Szko%C5%82a+Biznesu!5e0!3m2!1spl!2spl!4v1547260879213"></iframe>
        </div>


        <script>
            function scrollBottom() {
                scrollingElement = (document.scrollingElement || document.body)
                if (scrollingElement.scrollTop < scrollingElement.scrollHeight - window.innerHeight - 1) {
                    scrollingElement.scrollTop += 10
                    setTimeout(function () { scrollBottom() }, 1);
                }
            }

            function toggleLang() {
                if (localStorage['lang'] == 'pl')
                    localStorage['lang'] = 'en'
                else
                    localStorage['lang'] = 'pl'
                translateText(localStorage['lang'])
            }

            function translateText(curLang) {
                let textItems = []
                function getTextItems(tag) {
                    let items = document.getElementsByTagName(tag)
                    for (item of items) {
                        if (item.hasAttribute('text')) textItems.push(item)
                    }
                }

                getTextItems('a')
                getTextItems('button')
                getTextItems('p')
                getTextItems('pre')


                let json = `<?php echo file_get_contents('./json/lang.json') ?>`
                var jsonLang = JSON.parse(json);

                if (!localStorage['lang'])
                    localStorage['lang'] = 'en';

                for (item of textItems)
                    item.innerText = jsonLang[item.getAttribute('text')][curLang]

                let lang = document.getElementById('lang')
                lang.innerText += `: ${localStorage['lang'].toUpperCase()}`
            }
            translateText(localStorage['lang'] || 'en')
        </script>

        <script type="module">
            import { FlatMenu } from './js/FlatMenu.js'
            import { ItemContainer } from './js/ItemContainer.js'
            import { Item } from './js/Item.js'

            let json = `<?php echo file_get_contents('./json/cars.json') ?>`
            var jsonObj = JSON.parse(json);

            let flatMenu = new FlatMenu()
            let itemContainer = new ItemContainer()


            for (let key in jsonObj)
                flatMenu.addButton(key)

            flatMenu.addButtonEventListener(function (button) {
                itemContainer.clear()
                
                const key = button.innerText
                flatMenu.setActive(key)
                for (let value in jsonObj[key]) {
                    let item = new Item(value, jsonObj[key][value])
                    itemContainer.addItem(item)
                }

                translateText(localStorage['lang'])
            })       
        </script>
    </body>

</html>